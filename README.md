# CellarApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.1.1.

# Architecture Considerations

The project is divided by "Core" and "pages".

Inside the "Core" folder we have all "Services" and "Components" that we used on this project.

The "Pages" folder only contains the 3 page:

* Homepage
* Notes
* Detail View

On the "Assets" folder we have the images and styles. Regarding the "Styles" folder, I like to create a theme, 
where all the components and pages styles are grouped inside the same folder, instead of having them separated by the "x.component.scss" files.

This way we will create styles in a more generic way.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Built With

### Frameworks
* [Angular](https://angular.io/)
* [Angular Material](https://material.angular.io/)
* [Fontawesome](https://fontawesome.com/?from=io)

## Authors

* **Aarão Teixeira** - *Full Stack Developer* - [Aarão Teixeira](https://gitlab.com/aaraot)
