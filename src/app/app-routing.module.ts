import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
// Pages
import {HomepageComponent} from './pages/homepage/homepage.component';
import {DetailViewComponent} from './pages/detail-view/detail-view.component';
import {NotesComponent} from './pages/notes/notes.component';

const routes: Routes = [
  {
    path: '',
    component: HomepageComponent,
    pathMatch: 'full'
  },
  {
    path: 'notes',
    component: NotesComponent,
    pathMatch: 'full'
  },
  {
    path: 'detail-view/:id',
    component: DetailViewComponent,
    pathMatch: 'full'
  },
  {
    path: 'notes/:id',
    component: DetailViewComponent,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'disabled', initialNavigation: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
