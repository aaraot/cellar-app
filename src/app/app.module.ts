import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
// Modules
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatOptionModule} from '@angular/material/core';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatDialogModule} from '@angular/material/dialog';
// Components
import {AlertComponent} from './core/components/alert/alert.component';
import {DialogComponent} from './core/components/dialog/dialog.component';
import {ListComponent} from './core/components/list/list.component';
import {NavbarComponent} from './core/components/navbar/navbar.component';
import {PageHeaderComponent} from './core/components/page-header/page-header.component';
import {RatingComponent} from './core/components/rating/rating.component';
// Directives
import {LazyImgDirective} from './core/directives/lazy-img';
// Pages
import {AppComponent} from './app.component';
import {HomepageComponent} from './pages/homepage/homepage.component';
import {NotesComponent} from './pages/notes/notes.component';
import {DetailViewComponent} from './pages/detail-view/detail-view.component';

@NgModule({
  declarations: [
    // Directives
    LazyImgDirective,

    // Components
    AlertComponent,
    DialogComponent,
    ListComponent,
    NavbarComponent,
    PageHeaderComponent,
    RatingComponent,

    // Pages
    AppComponent,
    HomepageComponent,
    NotesComponent,
    DetailViewComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,

    // Modules
    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    MatOptionModule,
    MatCardModule,
    MatButtonModule,
    MatDialogModule
  ],
  exports: [
    // Modules
    MatFormFieldModule,
    MatInputModule,
    MatAutocompleteModule,
    MatOptionModule,
    MatCardModule,
    MatButtonModule,
    MatDialogModule,

    // Directives
    LazyImgDirective,

    // Components
    AlertComponent,
    DialogComponent,
    ListComponent,
    NavbarComponent,
    PageHeaderComponent,
    RatingComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
