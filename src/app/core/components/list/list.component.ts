import {ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
// Models
import {Beer} from '../../models/beer';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListComponent implements OnChanges {

  @Input() link: string = '';
  @Input() items: Beer[] = [];

  public autocomplete: FormControl = new FormControl();
  public sortOption: string = '';
  public categories: string[] = [];
  public options: string[] = [];
  public filteredOptions: Observable<string[]> = new Observable();
  public filteredItems: Beer[] = [];

  constructor() {
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.link = changes.link.currentValue;
    this.items = changes.items.currentValue;

    if (this.items.length > 0) {
      this.filteredOptions = this.autocomplete.valueChanges
        .pipe(
          startWith(''),
          map(search => this.filter(search))
        );
      this.filteredItems = this.items;

      // Pushes all item names to "autocomplete" options
      this.items
        .forEach(item => {
          if (item.name) {
            this.options.push(item.name);
          }
        });

      // Subscribes "autocomplete" changes
      // When the search it's empty it will show all items again
      this.autocomplete.valueChanges
        .subscribe(search => {
          if (search === '') {
            this.filteredItems = this.items;
          }
        });
    }
  }

  public sort(sort: string): void {
    this.sortOption = sort;
    if (sort === 'up') {
      this.filteredItems
        .sort((a, b) => this.sortAsc(a, b));
    } else {
      this.filteredItems
        .sort((a, b) => this.sortDesc(a, b));
    }
  }

  public filterList(search: string): void {
    this.filteredItems = this.items
      .filter(item => item.name === search);
  }

  private sortAsc(a: Beer, b: Beer): number {
    if (a.name && b.name) {
      return a.name > b.name ? 1 : -1;
    }
    return 0;
  }

  private sortDesc(a: Beer, b: Beer): number {
    if (a.name && b.name) {
      return b.name > a.name ? 1 : -1;
    }
    return 0;
  }

  private filter(search: string): Array<string> {
    return this.options
      .filter(option => option.toLowerCase().includes(search.toLowerCase()));
  }

}
