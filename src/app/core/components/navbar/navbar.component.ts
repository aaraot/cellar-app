import {ChangeDetectionStrategy, Component} from '@angular/core';
// Services
import {LocalStorageService} from '../../services/local-storage.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavbarComponent {

  constructor(private localStorageService: LocalStorageService) {
  }

  public resetScroll(): void {
    window.scrollTo(0, 0);
    this.localStorageService.setLocalStorage('homepageScroll', '0');
  }

}
