import {AfterViewInit, Directive, ElementRef, HostBinding, Input} from '@angular/core';
// Services
import {LocalStorageService} from '../services/local-storage.service';

@Directive({
  selector: 'img[appLazyLoad]'
})
export class LazyImgDirective implements AfterViewInit {

  @HostBinding('attr.src') srcAttr: string = '';
  @Input() src: string = '';

  constructor(
    private el: ElementRef,
    private localStorageService: LocalStorageService
  ) {
  }

  ngAfterViewInit(): void {
    if (this.localStorageService.isBrowser) {
      this.canLazyLoad() ? this.lazyLoadImage() : this.loadImage();
    }
  }

  private canLazyLoad(): boolean {
    return window && 'IntersectionObserver' in window;
  }

  private lazyLoadImage(): void {
    const obs = new IntersectionObserver(entries => {
      entries.forEach(({isIntersecting}) => {
        if (isIntersecting) {
          this.loadImage();
          obs.unobserve(this.el.nativeElement);
        }
      });
    });
    obs.observe(this.el.nativeElement);
  }

  private loadImage(): void {
    this.el.nativeElement.setAttribute('src', this.src);
  }

}
