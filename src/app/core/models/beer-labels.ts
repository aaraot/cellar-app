export interface BeerLabels {
  icon: string;
  medium: string;
  large: string;
  contentAwareIcon: string;
  contentAwareMedium: string;
  contentAwareLarge: string;
}
