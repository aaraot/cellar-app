export interface BeerStyleCategory {
  id: number;
  name: string;
  createDate: string;
}
