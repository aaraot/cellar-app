import {BeerLabels} from './beer-labels';
import {BeerStyle} from './beer-style';
import {Comment} from './comment';
import {BeerSpec} from './beer-spec';

export interface Beer {
  id?: string;
  name?: string;
  description?: string;
  labels?: BeerLabels;
  style?: BeerStyle;
  comments?: Comment[];
  notes?: string[];
  rate?: number[];
  specs?: BeerSpec[];
}
