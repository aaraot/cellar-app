import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
// Mock Data
import {BEERS} from './data/beers';

@Injectable({
  providedIn: 'root'
})
export class CellarService {

  // private key = '?key=bf48bc07f4b410257f76e3bd33f7f86e';

  constructor(private http: HttpClient) {
  }

  public getItems(): Observable<any> {
    return of(BEERS);
  }

  // public getItems(): Observable<any> {
  //   return this.http.get(`http://api.brewerydb.com/v2/beers${this.key}`);
  // }

  // getItem(id: number): any {
  //   return this.http.get(`http://api.brewerydb.com/v2/beer/${id}/${this.key}`);
  // }

}
