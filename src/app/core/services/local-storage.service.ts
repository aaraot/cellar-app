import {Inject, Injectable, PLATFORM_ID} from '@angular/core';
// Mock Data
import {BEERS} from './data/beers';
// Models
import {Beer} from '../models/beer';
import {Observable, of} from "rxjs";

export enum PlatformType {
  Browser = 'browser',
  Server = 'server'
}

@Injectable({
  providedIn: 'root'
})
export class LocalStorageService {

  public isBrowser: boolean = false;

  constructor(@Inject(PLATFORM_ID) private platformId: string) {
    this.isBrowser = platformId === PlatformType.Browser;
  }

  public getLocalStorageItems(): string {
    if (this.isBrowser) {
      return localStorage.getItem('items') || '';
    }
    return JSON.stringify(BEERS);
  }

  public getLocalStorageItem(id: string): Observable<Beer> {
    if (this.isBrowser) {
      const items = JSON.parse(this.getLocalStorageItems());
      return of(items.find((item: Beer) => item.id === id));
    }
    return of({});
  }

  public updateLocalStorageItem(id: string, newItem: Beer): any {
    const items = JSON.parse(this.getLocalStorageItems())
      .map((item: Beer) => {
        if (item.id === id) {
          return item = newItem;
        }
        return item;
      });
    this.setLocalStorage('items', JSON.stringify(items));
  }

  public getHomepageScroll(): any {
    if (this.isBrowser) {
      return localStorage.getItem('homepageScroll');
    }
  }

  public setScroll(event: any): void {
    if (!this.isBrowser) {
      return;
    }
    if (event.url && event.url !== '/') {
      this.setLocalStorage('homepageScroll', window.pageYOffset.toString());
      window.scrollTo(0, 0);
    } else {
      setTimeout(() => {
        window.scrollTo(0, +this.getHomepageScroll());
      }, 0);
    }
  }

  public setLocalStorage(key: string, value: any): void {
    if (this.isBrowser) {
      localStorage.setItem(key, value);
    }
  }

}
