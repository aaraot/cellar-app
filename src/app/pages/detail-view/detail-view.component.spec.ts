import {ComponentFixture, fakeAsync, getTestBed, TestBed, tick} from '@angular/core/testing';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from '../../app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
// Modules
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatOptionModule} from '@angular/material/core';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
// Components
import {AlertComponent} from '../../core/components/alert/alert.component';
import {DialogComponent} from '../../core/components/dialog/dialog.component';
import {ListComponent} from '../../core/components/list/list.component';
import {NavbarComponent} from '../../core/components/navbar/navbar.component';
import {PageHeaderComponent} from '../../core/components/page-header/page-header.component';
import {RatingComponent} from '../../core/components/rating/rating.component';
// Pages
import {AppComponent} from '../../app.component';
import {HomepageComponent} from '../homepage/homepage.component';
import {NotesComponent} from '../notes/notes.component';
import {DetailViewComponent} from './detail-view.component';
import {ActivatedRoute, Router} from '@angular/router';

import {BEERS} from '../../core/services/data/beers-w-notes';

describe('DetailViewComponent', () => {

  let component: DetailViewComponent;
  let fixture: ComponentFixture<DetailViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        // Components
        AlertComponent,
        DialogComponent,
        ListComponent,
        NavbarComponent,
        PageHeaderComponent,
        RatingComponent,

        // Pages
        AppComponent,
        HomepageComponent,
        NotesComponent,
        DetailViewComponent
      ],
      imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,

        // Modules
        MatFormFieldModule,
        MatInputModule,
        MatAutocompleteModule,
        MatOptionModule,
        MatCardModule,
        MatButtonModule,
        MatDialogModule
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              url: 'detail-view/c4f2KE',
              params: {id: 'c4f2KE'}
            }
          }
        }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    this.localStorageService.setLocalStorage('items', JSON.stringify(BEERS.data));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have as title "\'Murican Pilsner"', () => {
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.detail-view-header h1').textContent).toContain('\'Murican Pilsner');
  });

  it('should have 3 as rating', () => {
    const compiled = fixture.debugElement.nativeElement;
    compiled.querySelectorAll('.rating-wrapper .rating')[2].click();

    fixture.detectChanges();

    expect(compiled.querySelector('.detail-view-header h1 .rating').textContent).toContain('3/5');
  });

  it('should add 1st note', () => {
    const injector = getTestBed();
    const router = injector.get(Router);

    fixture.detectChanges();

    router.navigate(['/detail-view/c4f2KE'])
      .then(() => {
        fixture.componentInstance.isDetailPage = true;

        fixture.detectChanges();

        const compiled = fixture.debugElement.nativeElement;
        const addNote = compiled.querySelector('.detail-view-header .actions .note');

        addNote.click();

        fixture.detectChanges();
        fixture.whenStable();

        // TODO: MatDialog interactions

        fixture.detectChanges();
        fixture.whenStable();

        expect(compiled.querySelector('.detail-view-header .actions .note.active')).not.toBeUndefined();
      });
  });

  it('should add comment', fakeAsync(
    () => {
      const injector = getTestBed();
      const router = injector.get(Router);

      fixture.detectChanges();

      router.navigate(['/detail-view/c4f2KE'])
        .then(() => {
          fixture.componentInstance.isDetailPage = true;
          fixture.componentInstance._createForm();

          fixture.detectChanges();

          const compiled = fixture.debugElement.nativeElement;
          const addNote = compiled.querySelector('.mat-raised-button');

          addNote.click();

          fixture.detectChanges();
          fixture.whenStable();
          tick(0);

          const nameKey = 'name';
          const commentKey = 'comment';
          fixture.componentInstance.form.controls[nameKey].setValue('asd');
          fixture.componentInstance.form.controls[commentKey].setValue('asd');
          compiled.querySelector('form .actions .mat-primary').click();

          fixture.detectChanges();
          fixture.whenStable();
          tick(0);

          expect(compiled.querySelectorAll('.detail-view-comment').length).toBe(1);
        });
    }
  ));

});
