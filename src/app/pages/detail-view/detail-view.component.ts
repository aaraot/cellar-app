import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
// Services
import {CellarService} from '../../core/services/cellar.service';
import {LocalStorageService} from '../../core/services/local-storage.service';
// Models
import {Beer} from '../../core/models/beer';
import {Comment} from '../../core/models/comment';
// Components
import {DialogComponent} from '../../core/components/dialog/dialog.component';

@Component({
  selector: 'app-detail-view',
  templateUrl: './detail-view.component.html',
  styleUrls: ['./detail-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DetailViewComponent implements OnInit {

  public isDetailPage: boolean = true;
  public rating: number = 0;
  public specs: string[] = ['ibuMin', 'ibuMax', 'abvMin', 'abvMax', 'srmMin', 'srmMax', 'ogMin', 'fgMin', 'fgMax'];
  public comments: Comment[] = [];
  public notes: string[] = [];
  public items: Beer[] = [];
  public item: Beer = {};
  public commentSectionTitle: string = '';
  public commentSectionButtonText: string = '';
  public open: boolean = false;
  public form: FormGroup = new FormGroup({});

  private id: string = '';

  constructor(
    private route: ActivatedRoute,
    private cellarService: CellarService,
    private localStorageService: LocalStorageService,
    public dialog: MatDialog
  ) {
  }

  ngOnInit(): void {
    // Check if route is "Detail View"
    // This way we can define if we show the "Detail View" layout or "Notes" layout
    this.isDetailPage = this.route.toString().includes('detail-view');
    // Gets the item "id"
    // This way we can fetch the item that the user requested
    this.id = this.route.snapshot.params.id;

    // Fetching item
    // this.cellarService.getItem(this.id).subscribe(response => this.item = response);
    this.items = JSON.parse(this.localStorageService.getLocalStorageItems());
    this.localStorageService.getLocalStorageItem(this.id)
      .subscribe(response => {
        this.item = response;

        this.prepItem();
        this.setComments();
      });

    this.prepCommentSection();
    this.createForm();
  }

  public updateRate(rate: number): void {
    this.item.rate?.push(rate);
    this.setRating();
    this.save();
  }

  public createNote(): void {
    const dialogRef = this.dialog
      .open(DialogComponent, {
        width: '250px',
        data: {note: ''}
      });

    dialogRef.afterClosed()
      .subscribe(result => {
        this.item.notes?.unshift(result);
        this.save();
      });
  }

  public hasError(controlName: string, errorName: string): boolean {
    return this.form.controls[controlName].hasError(errorName);
  }

  public submit(formValues: any): void {
    if (this.form.valid) {
      this.isDetailPage ? this.item.comments?.unshift(formValues) : this.item.notes?.unshift(formValues.note);
      this.form.reset();
      this.open = false;

      this.setComments();
      this.save();
    }
  }

  private prepItem(): void {
    // Initiate item rating if it doesn't exist
    // And set it's value accordingly
    if (!this.item.rate) {
      this.item.rate = [];
    }
    this.setRating();

    // Get the item specs from "style" prop
    // It will fetch the "key" and "value" of each "style"
    this.item.specs = [];
    Object.keys(this.item.style || [])
      .map((key, index) => {
        if (this.specs.includes(key)) {
          if (this.item.style && this.item.specs) {
            this.item.specs?.push({key, value: this.item.style[key]});
          }
        }
      });

    // Initiate item comments if it doesn't exist
    if (!this.item.comments) {
      this.item.comments = [];
    }

    // Initiate item notes if it doesn't exist
    if (!this.item.notes) {
      this.item.notes = [];
    }
  }

  private setRating(): void {
    this.rating = this.item.rate ? parseFloat(this.calculateRating().toFixed(1)) : 0;
  }

  private calculateRating(): number {
    const reducer = (accumulator: number, currentValue: number) => accumulator + currentValue;
    return this.item.rate?.length ? this.item.rate.reduce(reducer) / this.item.rate.length : 0;
  }

  private prepCommentSection(): void {
    this.commentSectionTitle = this.isDetailPage ? 'Comments' : 'Notes';
    this.commentSectionButtonText = this.isDetailPage ? 'comment' : 'note';
  }

  private setComments(): void {
    switch (this.isDetailPage) {
      case false:
        this.notes = this.item.notes || [];
        break;
      default:
        this.comments = this.item.comments || [];
        break;
    }
  }

  private createForm(): void {
    let formGroup;
    if (this.isDetailPage) {
      formGroup = {
        name: new FormControl('', [Validators.required]),
        comment: new FormControl('', [Validators.required])
      };
    } else {
      formGroup = {
        note: new FormControl('', [Validators.required])
      };
    }
    this.form = new FormGroup(formGroup);
  }

  private save(): void {
    this.localStorageService.updateLocalStorageItem(this.id, this.item);
  }

}
