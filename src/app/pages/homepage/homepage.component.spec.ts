import {ComponentFixture, fakeAsync, TestBed, tick} from '@angular/core/testing';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from '../../app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HarnessLoader} from '@angular/cdk/testing';
import {MatAutocompleteHarness} from '@angular/material/autocomplete/testing';
import {TestbedHarnessEnvironment} from '@angular/cdk/testing/testbed';
// Modules
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatOptionModule} from '@angular/material/core';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
// Components
import {AlertComponent} from '../../core/components/alert/alert.component';
import {DialogComponent} from '../../core/components/dialog/dialog.component';
import {ListComponent} from '../../core/components/list/list.component';
import {NavbarComponent} from '../../core/components/navbar/navbar.component';
import {PageHeaderComponent} from '../../core/components/page-header/page-header.component';
import {RatingComponent} from '../../core/components/rating/rating.component';
// Pages
import {AppComponent} from '../../app.component';
import {HomepageComponent} from './homepage.component';
import {NotesComponent} from '../notes/notes.component';
import {DetailViewComponent} from '../detail-view/detail-view.component';

import {BEERS} from '../../core/services/data/beers-w-notes';

describe('HomepageComponent', () => {

  let component: HomepageComponent;
  let fixture: ComponentFixture<HomepageComponent>;
  let loader: HarnessLoader;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        // Components
        AlertComponent,
        DialogComponent,
        ListComponent,
        NavbarComponent,
        PageHeaderComponent,
        RatingComponent,

        // Pages
        AppComponent,
        HomepageComponent,
        NotesComponent,
        DetailViewComponent
      ],
      imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,

        // Modules
        MatFormFieldModule,
        MatInputModule,
        MatAutocompleteModule,
        MatOptionModule,
        MatCardModule,
        MatButtonModule,
        MatDialogModule
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    loader = TestbedHarnessEnvironment.loader(fixture);
    this.localStorageService.setLocalStorage('items', JSON.stringify(BEERS.data));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have as title "Beers"', () => {
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.page-header h1').textContent).toContain('Beers');
  });

  it('should have 50 items', () => {
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelectorAll('.mat-card').length).toEqual(50);
  });

  it('should search for "\'Murican Pilsner"', fakeAsync(
    async () => {
      const compiled = fixture.debugElement.nativeElement;
      const input = await loader.getHarness(MatAutocompleteHarness);

      await input.enterText('\'Murican Pilsner');

      fixture.detectChanges();

      await input.selectOption({});

      fixture.detectChanges();
      fixture.whenStable();
      tick(0);

      expect(compiled.querySelectorAll('.cellar-list a').length).toBe(1);
    }
  ));

  it('should sort alphabetically A to Z', fakeAsync(
    () => {
      const compiled = fixture.debugElement.nativeElement;
      const azSort = compiled.querySelectorAll('.cellar-filters-wrapper .sort span')[0];

      azSort.click();

      fixture.detectChanges();
      fixture.whenStable();
      tick(0);

      const firstCard = fixture.nativeElement.querySelectorAll('.cellar-list a')[0];

      expect(firstCard.querySelector('mat-card-title').textContent).toEqual('Alpha Dog Imperial IPA');
    }
  ));

  it('should sort alphabetically Z to A', fakeAsync(
    () => {
      const compiled = fixture.debugElement.nativeElement;
      const zaSort = compiled.querySelectorAll('.cellar-filters-wrapper .sort span')[1];

      zaSort.click();

      fixture.detectChanges();
      fixture.whenStable();
      tick(0);

      const firstCard = fixture.nativeElement.querySelectorAll('.cellar-list a')[0];

      expect(firstCard.querySelector('mat-card-title').textContent).toEqual('\'Murican Pilsner');
    }
  ));

});
