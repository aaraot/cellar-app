import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {take} from 'rxjs/operators';
// Services
import {CellarService} from '../../core/services/cellar.service';
// Models
import {Beer} from '../../core/models/beer';
// Services
import {LocalStorageService} from '../../core/services/local-storage.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomepageComponent implements OnInit {

  public items: Beer[] = [];

  constructor(
    private cellarService: CellarService,
    private localStorageService: LocalStorageService
  ) {
  }

  ngOnInit(): void {
    // Fetch items
    // Checks if there is data on the "localStorage"
    // If it's empty it will fetch the data from the api and then save it in the "localStorage"
    // Otherwise it will fetch the data from the "localStorage"
    // this.cellarService.getItems().subscribe(response => this.items = response);
    if (!this.localStorageService.getLocalStorageItems()) {
      this.cellarService.getItems()
        .pipe(take(1))
        .subscribe(response => this.items = response.data);
      this.localStorageService.setLocalStorage('items', JSON.stringify(this.items));
    } else {
      this.items = JSON.parse(this.localStorageService.getLocalStorageItems());
    }
  }

}
