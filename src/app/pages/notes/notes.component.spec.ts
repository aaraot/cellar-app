import {ComponentFixture, fakeAsync, getTestBed, TestBed, tick} from '@angular/core/testing';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from '../../app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HarnessLoader} from '@angular/cdk/testing';
import {TestbedHarnessEnvironment} from '@angular/cdk/testing/testbed';
import {MatAutocompleteHarness} from '@angular/material/autocomplete/testing';
// Modules
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatOptionModule} from '@angular/material/core';
import {MatCardModule} from '@angular/material/card';
import {MatButtonModule} from '@angular/material/button';
import {MatDialogModule} from '@angular/material/dialog';
// Components
import {AlertComponent} from '../../core/components/alert/alert.component';
import {DialogComponent} from '../../core/components/dialog/dialog.component';
import {ListComponent} from '../../core/components/list/list.component';
import {NavbarComponent} from '../../core/components/navbar/navbar.component';
import {PageHeaderComponent} from '../../core/components/page-header/page-header.component';
import {RatingComponent} from '../../core/components/rating/rating.component';
// Pages
import {AppComponent} from '../../app.component';
import {HomepageComponent} from '../homepage/homepage.component';
import {NotesComponent} from './notes.component';
import {DetailViewComponent} from '../detail-view/detail-view.component';

import {BEERS} from '../../core/services/data/beers-w-notes';

describe('NotesComponent', () => {

  let component: NotesComponent;
  let fixture: ComponentFixture<NotesComponent>;
  let loader: HarnessLoader;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        // Components
        AlertComponent,
        DialogComponent,
        ListComponent,
        NavbarComponent,
        PageHeaderComponent,
        RatingComponent,

        // Pages
        AppComponent,
        HomepageComponent,
        NotesComponent,
        DetailViewComponent
      ],
      imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,

        // Modules
        MatFormFieldModule,
        MatInputModule,
        MatAutocompleteModule,
        MatOptionModule,
        MatCardModule,
        MatButtonModule,
        MatDialogModule
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              params: {id: 'c4f2KE'}
            }
          }
        }
      ]
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    loader = TestbedHarnessEnvironment.loader(fixture);
    localStorage.setItem('items', JSON.stringify(BEERS.data));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have as title "Notes"', () => {
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.page-header h1').textContent).toContain('Notes');
  });

  it('should search for "\'Murican Pilsner"', fakeAsync(
    async () => {
      const compiled = fixture.debugElement.nativeElement;
      const input = await loader.getHarness(MatAutocompleteHarness);

      await input.enterText('\'Murican Pilsner');

      fixture.detectChanges();

      await input.selectOption({});

      fixture.detectChanges();
      fixture.whenStable();
      tick(0);

      expect(compiled.querySelectorAll('.cellar-list a').length).toBe(1);
    }
  ));

  it('should sort alphabetically A to Z', fakeAsync(
    () => {
      const compiled = fixture.debugElement.nativeElement;
      const azSort = compiled.querySelectorAll('.cellar-filters-wrapper .sort span')[0];

      azSort.click();

      fixture.detectChanges();
      fixture.whenStable();
      tick(0);

      const firstCard = fixture.nativeElement.querySelectorAll('.cellar-list a')[0];

      expect(firstCard.querySelector('mat-card-title').textContent).toEqual('11.5° PLATO');
    }
  ));

  it('should sort alphabetically Z to A', fakeAsync(
    () => {
      const compiled = fixture.debugElement.nativeElement;
      const zaSort = compiled.querySelectorAll('.cellar-filters-wrapper .sort span')[1];

      zaSort.click();

      fixture.detectChanges();
      fixture.whenStable();
      tick(0);

      const firstCard = fixture.nativeElement.querySelectorAll('.cellar-list a')[0];

      expect(firstCard.querySelector('mat-card-title').textContent).toEqual('\'Murican Pilsner');
    }
  ));

  it('should go to detail view', fakeAsync(
    () => {
      const injector = getTestBed();
      const router = injector.get(Router);
      const detailViewFixture = TestBed.createComponent(DetailViewComponent);

      detailViewFixture.detectChanges();

      router.navigate(['/notes/c4f2KE'])
        .then(() => {
          detailViewFixture.detectChanges();
          expect(detailViewFixture.debugElement.nativeElement.querySelector('.detail-view-header h1 span').textContent).toContain('\'Murican Pilsner');
        });
    }
  ));

  it('should add note', fakeAsync(
    () => {
      const injector = getTestBed();
      const router = injector.get(Router);
      const detailViewFixture = TestBed.createComponent(DetailViewComponent);

      detailViewFixture.detectChanges();

      router.navigate(['/notes/c4f2KE'])
        .then(() => {
          detailViewFixture.detectChanges();

          const compiled = detailViewFixture.debugElement.nativeElement;
          const addNote = compiled.querySelector('.mat-raised-button');

          addNote.click();

          detailViewFixture.detectChanges();
          detailViewFixture.whenStable();
          tick(0);

          const key = 'note';
          detailViewFixture.componentInstance.form.controls[key].setValue('asd2');
          compiled.querySelector('form .actions .mat-primary').click();

          detailViewFixture.detectChanges();
          detailViewFixture.whenStable();
          tick(0);

          expect(compiled.querySelectorAll('.detail-view-comment').length).toBe(2);
        });
    }
  ));

});
