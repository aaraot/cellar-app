import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
// Models
import {Beer} from '../../core/models/beer';
// Services
import {LocalStorageService} from '../../core/services/local-storage.service';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NotesComponent implements OnInit {

  public items: Beer[] = [];

  constructor(
    private localStorageService: LocalStorageService
  ) {
  }

  ngOnInit(): void {
    this.items = JSON.parse(this.localStorageService.getLocalStorageItems());
    this.items = this.items ? this.items.filter(item => item.notes?.length) : [];
  }

}
